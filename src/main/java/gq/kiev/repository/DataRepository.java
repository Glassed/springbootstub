package gq.kiev.repository;

import gq.kiev.entity.DomainObject;
import java.util.Set;

/**
 *
 * @author alexander
 */
public interface DataRepository<V extends DomainObject>{
    
    void persist(V object);
    
    void delete(V object);
    
    Set<String> getRandomData();    
    
    
}
