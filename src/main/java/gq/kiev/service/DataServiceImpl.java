package gq.kiev.service;

import gq.kiev.entity.Data;
import gq.kiev.repository.DataRepository;
import java.util.Set;
import java.util.UUID;
import org.hibernate.jpa.internal.EntityManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author alexander
 */
@Service("dataService")
public class DataServiceImpl implements DataService {
    
    @Autowired
    @Qualifier("dataRepository")
    private DataRepository DataRepository;
    
    @Override
    public boolean persist(String problem) {
        try{
            DataRepository.persist(new Data(UUID.randomUUID(), problem));
            return true;
        } catch(Exception ex){
            EntityManagerImpl.LOG.error("ERROR SAVING DATA: "+ ex.getMessage(), ex);
            return false;
        }  
    }

    @Override
    public Set<String> getRandomData() {
        return DataRepository.getRandomData();
    }

}
