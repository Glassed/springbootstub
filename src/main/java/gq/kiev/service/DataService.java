package gq.kiev.service;

import java.util.Set;

/**
 *
 * @author alexander
 */
public interface DataService {
    boolean persist(String problem);
    Set<String> getRandomData();    
}
