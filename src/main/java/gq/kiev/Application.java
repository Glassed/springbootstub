package gq.kiev;

import gq.kiev.config.JpaConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan("gq.kiev")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] 
                {Application.class, JpaConfig.class}, args);
	}
}
