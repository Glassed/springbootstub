Для запуска проекта понадобится установленный PostgreSQL,
создана база данных futureapp_data
с пользователем app_rw
и паролем 0cvfb09
У этого пользователя как минимум должны быть права SELECT,UPDATE

в ней создана таблица следующим образом :

CREATE TABLE yourapp_data
(
  data_id uuid NOT NULL,
  data_description character varying(100) NOT NULL,
  CONSTRAINT yourapp_data_pk PRIMARY KEY (data_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE yourapp_data
  OWNER TO postgres;